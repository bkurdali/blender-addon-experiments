'''
Copyright (C) 2021 Bassam Kurdali

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

bl_info = {
    "name": "Dark Window",
    "author": "Bassam Kurdali",
    "version": (1, 0),
    "blender": (2, 90, 0),
    "location": "",
    "description": "Sets dark GTK Theme for Blender Window",
    "warning": "Only sets main window, not new windows or preference, drivers, etc. so far",
    "doc_url": "",
    "category": "System",
}


import bpy
import os

def register():
   os.system('xprop -f _GTK_THEME_VARIANT 8u -set _GTK_THEME_VARIANT "dark" -name Blender')


def unregister():
    os.system('xprop -f _GTK_THEME_VARIANT 8u -remove _GTK_THEME_VARIANT -name Blender')


if __name__ == "__main__":
    register()
