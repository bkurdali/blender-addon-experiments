# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110 - 1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
#
# 2014 Bassam Kurdali

'''
Interaction Changes:

5- make min/max absolute
6- allow ghostly zone outside window
7- close button in addition to ESC

Visual changes:
1- make PNG button for rounded corners, header, close button
2- keep a transparent outer rim as a pad
3- draw several quads with the different images/colors for UI
4- draw thin line as border
5- better/ more selection options, pair with a real rig (rigify?)
'''

import bpy
import bgl
import blf
import os
import xml.etree.ElementTree as ET
from bpy.types import SpaceView3D
from mathutils import Vector
from mathutils.geometry import intersect_point_quad_2d
from pprint import pprint


def get_svg_data(path):
    ''' get data from svg file, both pixel dimensions and selection bounds '''
    tree = ET.parse(path)
    root = tree.getroot()
    root_dict = dict(root.items())
    svg_height = float(root_dict['height'])
    svg_width = float(root_dict['width'])

    mylayer = [
        itm for itm in root if 'boundingboxes' in [
            value[1] for value in itm.items()]][0]
    bounds = {}
    for ellipse in mylayer:
        if 'ellipse' in mylayer.tag or True:
            stuff = dict(ellipse.items())
            name = stuff['id']
            x = float(stuff['cx'])
            y = float(stuff['cy'])
            bounds[name] = (x,y)
    names = bounds.keys()
    toplevel = list(set([name[:-2] for name in names]))
    svg_data = {}
    for name in toplevel:
        pointlist = []
        for point in bounds:
            if name in point:
                pointlist.append((int(point[-1]), bounds[point]))
        pointlist.sort()
        svg_data[name] = {'select':[], 'bounds':[item[1] for item in pointlist]}
    return (svg_data, svg_height, svg_width)


def get_armature_data(svg_data):
    ''' Stuff bones into svg data for selection '''
    # Let's hardcode some bones to select (for now):
    bone_data = {'arm_L': [
                    'upper_arm.L', 'forearm.L', 'hand.L', 'palm.01.L',
                    'f_index.01.L', 'f_index.02.L', 'f_index.03.L', 'thumb.01.L',
                    'thumb.02.L', 'thumb.03.L', 'palm.02.L', 'f_middle.01.L',
                    'f_middle.02.L', 'f_middle.03.L', 'palm.03.L', 'f_ring.01.L',
                    'f_ring.02.L', 'f_ring.03.L', 'palm.04.L', 'f_pinky.01.L',
                    'f_pinky.02.L', 'f_pinky.03.L'],
                 'arm_R': [
                    'upper_arm.R', 'forearm.R', 'hand.R', 'palm.01.R',
                    'f_index.01.R', 'f_index.02.R', 'f_index.03.R', 'thumb.01.R',
                    'thumb.02.R', 'thumb.03.R', 'palm.02.R', 'f_middle.01.R',
                    'f_middle.02.R', 'f_middle.03.R', 'palm.03.R', 'f_ring.01.R',
                    'f_ring.02.R', 'f_ring.03.R', 'palm.04.R', 'f_pinky.01.R',
                    'f_pinky.02.R', 'f_pinky.03.R'],
                'body': ['hips', 'spine', 'chest', 'shoulder.L', 'shoulder.R'],
                'head': ['head', 'neck'],
                'leg_L': [
                    'thigh.L', 'shin.L', 'foot.L', 'toe.L', 'heel.L', 'heel.02.L'],
                'leg_R': [
                    'thigh.R', 'shin.R', 'foot.R', 'toe.R', 'heel.R', 'heel.02.R']}
    for body_part in bone_data:
        svg_data[body_part]['select'].extend(bone_data[body_part])

# Currently svg data has to live with blend file
path = os.path.join(
    os.path.split(bpy.context.blend_data.filepath)[0], 'picker.svg')    
svg_data, svg_height, svg_width = get_svg_data(path)
get_armature_data(svg_data)


class selector():
    ''' Helper class to make selecting easier '''

    def __init__(self, name, data):
        self.name = name
        self.bounds = [bound for bound in data['bounds']]
        self.selects = data['select']

    def detect(self, mouse, offset, size):
        ''' return 0 / 1 if we click inside the boundbox '''
        bounds = [
            Vector((
                size[0] * bound[0] / svg_width + offset[0],
                size[1] * (svg_height - bound[1]) / svg_height + offset[1]))
            for bound in self.bounds]
        return intersect_point_quad_2d(
            Vector(mouse), bounds[0], bounds[1], bounds[2], bounds[3])    

    def select(self, context):
        ''' select associated bones '''
        bones = context.active_object.data.bones
        for bone in bones:
            if bone.name in self.selects:
                bone.select = True
            else:
                bone.select = False
        bones.active = bones[self.selects[0]] # First Bone in List is active

    def detect_and_select(self, context, mouse, offset, size):
        ''' save caller some time '''
        if self.detect(mouse, (offset[0], offset[1]), (size[0], size[1])):
            print('selecting')
            self.select(context)
            return(True)
        else:
            return(False)

selectors = [selector(itm, svg_data[itm]) for itm in svg_data]

non_blocking = ['RIGHTMOUSE', 'MIDDLEMOUSE', 'MOUSEROTATE']        
def draw_callback_px(self, context):
    ''' Actually draw picker on screen '''
    props = self.properties
    x, y = props.location
    width , height = props.size
    title_height = props.title_height

    img = bpy.data.images[props.image]

    bgl.glEnable(bgl.GL_BLEND)
    bgl.glColor3f(1.0, 1.0, 1.0)
    bgl.glEnable(bgl.GL_TEXTURE_2D)
    bgl.glBindTexture (bgl.GL_TEXTURE_2D, img.bindcode)

    # Draw a textured quad
    bgl.glBegin(bgl.GL_QUADS)
    bgl.glTexCoord2f(0.0, 0.0)
    bgl.glVertex2i(x,y)
    bgl.glTexCoord2f(1.0, 0.0)
    bgl.glVertex2i(x + width, y)
    bgl.glTexCoord2f(1.0, 1.0)
    bgl.glVertex2i(x + width, y + height)
    bgl.glTexCoord2f(0.0, 1.0)
    bgl.glVertex2i(x, y + height)
    bgl.glEnd()

    bgl.glColor4f(0.0, 0.0, 0.0 ,0.5)

    # Draw Title Seperator
    bgl.glBegin(bgl.GL_LINE_STRIP)
    bgl.glVertex2i(x,y + height - title_height)
    bgl.glVertex2i(x + width, y + height - title_height)
    bgl.glEnd()
    
    # Uncomment the following to debug bounds:
    
    bgl.glBegin(bgl.GL_QUADS)
    bgl.glVertex2i(x,y)
    bgl.glVertex2i(x+10,y)
    bgl.glVertex2i(x+10, y+10)
    bgl.glVertex2i(x, y+10)
    bgl.glEnd()    
    for selector in selectors:
        
        bgl.glBegin(bgl.GL_QUADS)
        for point in selector.bounds:
            bgl.glVertex2i(
                x + int(point[0] * width / svg_width),
                y + int(height - point[1] * height / svg_height))
        bgl.glEnd()
    font_id = 0
    blf.position(font_id, x + width, y, 0)
    blf.size(font_id, 12, 72)
    blf.draw(font_id, props.debug)
            
    
    # restore opengl defaults
    bgl.glLineWidth(1)
    bgl.glDisable(bgl.GL_BLEND)
    bgl.glColor4f(0.0, 0.0, 0.0, 1.0)


def test_rect(mouse, x, y):
    ''' test if mouse falls in rectangular bounds '''
    return intersect_point_quad_2d(
        Vector(mouse),
        Vector((x[0], y[0])),
        Vector((x[1], y[0])),
        Vector((x[1], y[1])),
        Vector((x[0], y[1])))

# operator global memory:
global_location = [0, 0]
global_size = [256, 256]

    
class PickOperator(bpy.types.Operator):
    '''Use an image to pick Controls'''
    bl_idname = "pose.picker"
    bl_label = "Control Picker"
    bl_options = {'PRESET'}
    location = bpy.props.IntVectorProperty(
        default=global_location, size=2, subtype='TRANSLATION')
    old_location = bpy.props.IntVectorProperty(
        default=global_location, size=2, subtype='TRANSLATION')
    offset = bpy.props.IntVectorProperty(
        default=(0,0), size=2, subtype='TRANSLATION')
    size = bpy.props.IntVectorProperty(
        default=global_size, size=2)
    old_size = bpy.props.IntVectorProperty(
        default=global_size, size=2)
    title_height = bpy.props.IntProperty(default=14)
    grip_margin = bpy.props.IntProperty(default=10)
    moving = bpy.props.BoolProperty(default=False)
    scaling = bpy.props.IntVectorProperty(default=(0, 0), size=2)
    image = bpy.props.StringProperty(default='picker')
    debug = bpy.props.StringProperty(default='debug') # optional debug string

    def modal(self, context, event):
        mouse_x = event.mouse_region_x
        mouse_y = event.mouse_region_y
        props = self.properties
        title_height = props.title_height
        grip_margin = props.grip_margin
        props.debug = "{},{},{}".format(event.type, event.value, event.unicode)
        context.area.tag_redraw() 
        # pad the bounds a bit if we are moving or scaling:
        bot_pad = props.scaling[1] * 40
        horiz_pad = props.scaling[0] * 40
        top_pad = 40 if props.moving else 0
        # if we're out of bounds, just pass through, ESC fully quits
        if event.type in {'ESC'} or not test_rect(
                (mouse_x, mouse_y),
                (props.location[0] + min(0, horiz_pad),
                props.location[0] + props.size[0] + max(0,horiz_pad)),
                (props.location[1] + bot_pad,
                props.location[1] + props.size[1] + top_pad)):
            props.moving = False
            props.scaling[0] = props.scaling[1] = 0
            context.window.cursor_modal_restore()
            if event.type in {'ESC'}: 
                SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
                return {'FINISHED'}
            return {'PASS_THROUGH'}
        # if we've released a mouse button, stop moving/scaling         
        if event.value == 'RELEASE':
            if props.moving:
                global_location[0] = props.location[0]
                global_location[1] = props.location[1]
                props.moving = False
                context.window.cursor_modal_restore()
            if props.scaling[0] or props.scaling[1]:
                props.scaling[0] = props.scaling[1] = 0
                global_size[0] = props.size[0]
                global_size[1] = props.size[1]
                context.window.cursor_modal_restore()
        # if we're moving the mouse, respond if we are doing something
        elif event.type in ('MOUSEMOVE', 'INBETWEEN_MOUSEMOVE'):
            if props.moving:
                props.location[0] =\
                    mouse_x + props.offset[0] - props.size[0] // 2
                props.location[1] =\
                    mouse_y + props.offset[1] - props.size[1] // 2
            else:
                if props.scaling[0]:
                    new_size = props.old_size[0] + props.scaling[0] *\
                        (mouse_x - props.offset[0])     
                    props.size[0] = min(max(
                        svg_width / 4, new_size), svg_width * 3)
                    if props.scaling[0] < 0:
                        props.location[0] =\
                            props.old_location[0] - props.size[0] +\
                            props.old_size[0]
                if props.scaling[1]:
                    new_size = props.old_size[1] + props.scaling[1] *\
                        (mouse_y - props.offset[1])     
                    props.size[1] = min(max(
                        svg_height / 4, new_size), svg_height * 3)
                    props.location[1] = props.old_location[1] - props.size[1] +\
                        props.old_size[1]
        # region check for header (move) and 3 grips (scale)
        in_header = test_rect(
            (mouse_x, mouse_y),
            (props.location[0], props.location[0] + props.size[0]),
            (props.location[1] + props.size[1] - title_height,
            props.location[1] + props.size[1]))
        grip_bottom = test_rect(
            (mouse_x, mouse_y),
            (props.location[0], props.location[0] + props.size[0]),
            (props.location[1], props.location[1] + grip_margin))
        grip_left = test_rect(
            (mouse_x, mouse_y),
            (props.location[0], props.location[0] + grip_margin),
            (props.location[1], props.location[1] + props.size[1]))
        grip_right = test_rect(
            (mouse_x, mouse_y),
            (props.location[0] + props.size[0] - grip_margin,
            props.location[0] + props.size[0]),
            (props.location[1], props.location[1] + props.size[1]))
        # highlight the mouse icon if we are over a hotspot
        if in_header and not props.scaling[0] and not props.scaling[1]:
            context.window.cursor_modal_set('HAND')
        elif not props.moving:
            if grip_left or grip_right:
                context.window.cursor_modal_set(('SCROLL_X','SCROLL_XY')[grip_bottom])
            elif grip_bottom:
                context.window.cursor_modal_set('SCROLL_Y')
            else:
                context.window.cursor_modal_restore()
        # if we click the left mouse down, take appropriate action:
        if event.type == 'LEFTMOUSE' and event.value == 'PRESS':
            if not props.moving and in_header:
                context.window.cursor_modal_set('HAND')
                props.offset[0] =\
                    props.location[0] - mouse_x + props.size[0] // 2
                props.offset[1] =\
                    props.location[1] - mouse_y + props.size[1] // 2
                props.moving = True
            elif grip_bottom or grip_left or grip_right:
                if not props.scaling[1] and grip_bottom:
                    props.scaling[1] = -1
                    props.old_location[1] = props.location[1]
                    props.old_size[1] = props.size[1]
                    props.offset[1] = mouse_y
                if not props.scaling[0] and grip_left:
                    props.scaling[0] = -1
                    props.old_size[0] = props.size[0]
                    props.old_location[0] = props.location[0]
                    props.offset[0] = mouse_x
                elif not props.scaling[0] and grip_right:
                    props.scaling[0] = 1
                    props.old_location[0] = props.location[0]
                    props.old_size[0] = props.size[0]
                    props.offset[0] = mouse_x
            else:
                for selector in selectors:
                    if selector.detect_and_select(
                            context, (mouse_x, mouse_y),
                            props.location, props.size):
                        break
        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        props = self.properties
        if global_location == [0, 0] and True:
            props.location[0] = props.old_location[0] =\
                event.mouse_region_x -  self.properties.size[0] // 2
            props.location[1] = props.old_location[1] =\
                event.mouse_region_y  - self.properties.size[0] // 2
        else:
            props.location[0] = props.old_location[0] = global_location[0]
            props.location[1] = props.old_location[1] = global_location[1]
        props.size[0] = props.old_size[0] = global_size[0]
        props.size[1] = props.old_size[1] = global_size[1]
        img = bpy.data.images[self.properties.image]
        img.gl_load(bgl.GL_NEAREST,bgl.GL_NEAREST)
        bgl.glTexParameteri(
            bgl.GL_TEXTURE_2D, bgl.GL_TEXTURE_MAG_FILTER, bgl.GL_LINEAR)
        if context.area.type == 'VIEW_3D':
            # the arguments we pass the the callback
            args = (self, context)
            # Add the region OpenGL drawing callback
            # draw in view space with 'POST_VIEW' and 'PRE_VIEW'
            self._handle = SpaceView3D.draw_handler_add(
                draw_callback_px, args, 'WINDOW', 'POST_PIXEL')
            context.window_manager.modal_handler_add(self)
            return {'RUNNING_MODAL'}
        else:
            self.report({'WARNING'}, "View3D not found, cannot run operator")
            return {'CANCELLED'}


def register():
    bpy.utils.register_class(PickOperator)
    wm = bpy.context.window_manager
    kc = wm.keyconfigs.addon
    if 'Pose' in kc.keymaps:
        km = kc.keymaps['Pose']
    else:
        km = kc.keymaps.new(name='Pose')
    kmi = km.keymap_items.new(
        PickOperator.bl_idname, 'F', 'PRESS', ctrl=True)


def unregister():
    kc = wm.keyconfigs.addon
    for km in kc.keymaps:
        for kmi in km.keymap_items:
            if kmi.idname == PickOperator.bl_idname:
                km.keymap_items.remove(kmi)
    bpy.utils.unregister_class(PickOperator)

if __name__ == "__main__":
    register()

